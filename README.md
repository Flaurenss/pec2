# Introduction

Welcome to "Mickey Fantasia", a platform game inspired by the classic Fantasia released for MegaDrive about 1991.
In this case we embody the skin of the famous mouse to overcome the multiple obstacles that we will encounter as well as collecting gems and raffling / overcoming enemies in order to find the magical artifact.

## Game instructions

"Mickey Fantasia" is a paltform game, the user must advance through the terrain as he is allowed to defeat enemies by jumping on his head, collecting block gems (which will shine indicating that they contain a reward) by hitting with the Character's head at the bottom of the blocks.
The player has 400 seconds and 3 lives to reach the goal (where the magic device is located), otherwise the game will end with GAME OVER.

The game can be paused at any time, allowing the player to rest or return to the main menu.

## Dev information

You can download the game from:
https://braverobot.itch.io/mickey-fantasia



