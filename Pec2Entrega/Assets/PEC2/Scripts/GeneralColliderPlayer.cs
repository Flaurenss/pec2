﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralColliderPlayer : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if((GetComponentInParent<PlayerController>().isJumping))
        {
            if(Input.GetKey(KeyCode.Space))
            {
                Debug.Log("cant jump");
                GetComponentInParent<PlayerController>().canJump = false;
            }
            
        }
    }
}
