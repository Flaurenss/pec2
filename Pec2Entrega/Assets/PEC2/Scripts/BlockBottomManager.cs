﻿using UnityEngine;

public class BlockBottomManager : MonoBehaviour {

    public bool gemSoundToDisplay;

    Animator blockAnimator;
    private int rewardNumber = 5;
    private string blockTag;
    private AudioSource gemSound;
    private bool rewardGave;
	// Use this for initialization
	void Start ()
    {
        rewardGave = false;
        gemSoundToDisplay = false;
        gemSound = GetComponent<AudioSource>();
        blockAnimator = GetComponentInParent<Animator>();
        blockTag = transform.parent.tag;
	}
	
	// Update is called once per frame
	void Update () {
		if(gemSoundToDisplay)
        {
            DisplayCoinSound();
        }
	}

    public void DisplayCoinSound()
    {
        gemSound.Play();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.transform.gameObject.tag == "Player")
        {
            switch(blockTag)
            {
                case "SpecialBlockv2":
                    if(rewardNumber > 0)
                    {
                        blockAnimator.SetTrigger("Hit");
                        GamePlay.gems++;
                        GamePlay.punctuation += 200;
                        rewardNumber--;
                        if(rewardNumber == 0)
                        {
                            blockAnimator.SetBool("NoRewards", true);
                        }
                    }
                    break;
                default:
                    if(!rewardGave)
                    {
                        blockAnimator.SetTrigger("Hit");
                        GamePlay.gems++;
                        GamePlay.punctuation += 200;
                        rewardGave = true;
                    }
                    break;
            }
        }
    }
}
