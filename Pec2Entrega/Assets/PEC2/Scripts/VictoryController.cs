﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VictoryController : MonoBehaviour {

    public GameObject victoryScene;
    public Text totalPoints;
    /// <summary>
    /// var used to allow other scripts comunicate that the canvas info must be reset
    /// </summary>
    public static bool finalCountPunctuation;


	void Start () {
        finalCountPunctuation = false;
    }
	
	// Update is called once per frame
	void Update () {
		if(finalCountPunctuation)
        {
            totalPoints.text = GamePlay.punctuation.ToString() + " points";
            victoryScene.SetActive(true);
        }
	}

    public void Restart()
    {
        GamePlay.resetCanvasValues = true;
        SceneManager.LoadScene("GameScene");
    }

    public void Exit()
    {
        GamePlay.resetCanvasValues = true;
        SceneManager.LoadScene("MainMenu");
    }
}
