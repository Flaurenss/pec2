﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour {

    // Use this for initialization
    AudioSource audioSource;
	void Start () {
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("escape"))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    public void PLay()
    {
        audioSource.Play();
        SceneManager.LoadScene("GameScene");
    }

    public void Exit()
    {
        audioSource.Play();
        SceneManager.LoadScene("MainMenu");
    }
}
