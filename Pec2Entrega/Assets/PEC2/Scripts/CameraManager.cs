﻿using UnityEngine;

public class CameraManager : MonoBehaviour {

    public GameObject player;
    public GameObject leftLimit;
    public GameObject rightLimitChecker;
    public LayerMask whatIsEnd;
    public static Bounds cameraBoundsUpdated;

    private Vector3 newCameraPos;
    private Vector3 newLimitPos;
    private Camera maincCamera;
    private float limitMapCheckRadius = 0.1f;
    // Use this for initialization
    void Start()
    {
        maincCamera = GetComponent<Camera>();
        var bounds = OrtographicBound(maincCamera);
        leftLimit.transform.position = new Vector3(bounds.min.x, leftLimit.transform.position.y, leftLimit.transform.position.z);
        rightLimitChecker.transform.position = new Vector3(bounds.max.x, leftLimit.transform.position.y, leftLimit.transform.position.z);
    }

    /// <summary>
    /// Gets the bounds of the camera in order to place the invisible wall on the left side
    /// </summary>
    /// <param name="camera"></param>
    /// <returns></returns>
    static Bounds OrtographicBound(Camera camera)
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = camera.orthographicSize * 2;
        Bounds bounds = new Bounds(camera.transform.position,new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
        return bounds;
    }

    /// <summary>
    /// Updates the camera position in order to only follow the player on the x axis and stop on the map final limit
    /// </summary>
    void LateUpdate()
    {
        cameraBoundsUpdated = OrtographicBound(maincCamera);
        if (player.transform.position.x > transform.position.x)
        {
            if (Physics2D.OverlapCircleAll(rightLimitChecker.transform.position, limitMapCheckRadius, whatIsEnd).Length == 0)
            {
                newCameraPos.x = player.transform.position.x;
            }
        }
        else
        {
            newCameraPos.x = transform.position.x;
        }
        newCameraPos.y = transform.position.y;
        newCameraPos.z = transform.position.z;
        transform.position = newCameraPos;
    }
}
