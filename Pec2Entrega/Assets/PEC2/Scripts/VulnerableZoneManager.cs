﻿using UnityEngine;

public class VulnerableZoneManager : MonoBehaviour {

    /// <summary>
    /// Confirm the enemy (parent) when is beeing killed
    /// </summary>
    /// <param name="other">Param to know who is colliding with the vulnerable zone of the enemy</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        // Debug.Log("Enemy head hit by " + other.gameObject.tag);
        // if (other.gameObject.transform.tag == "PlayerFeetPos")
        // {
        //     // GetComponentInParent<EnemyManager>().killed = true;
        //     // GamePlay.punctuation += 100;
        // }
    }
}
