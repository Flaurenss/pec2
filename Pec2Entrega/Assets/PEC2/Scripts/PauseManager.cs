﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour {

    public GameObject pauseMenu;

	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown("escape"))
        {
            if(!pauseMenu.activeSelf)
            {
                Time.timeScale = 0;
                pauseMenu.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                pauseMenu.SetActive(false);
            }
        }

        // if(Input.GetKeyDown("r"))
        // {
        //    SceneManager.LoadScene("GameScene"); 
        // }
    }

    /// <summary>
    /// Unpause the game and returns to the MainMenu
    /// </summary>
    public void Exit()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        GamePlay.resetCanvasValues = true;
        SceneManager.LoadScene("MainMenu");
    }
}
