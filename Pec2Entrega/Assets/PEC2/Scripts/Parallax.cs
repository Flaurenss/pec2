﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour {

    public GameObject Cam;
    public float ParallaxEffect;

    private float length, startPos;


	// Use this for initialization
	void Start ()
    {
        startPos = transform.position.x;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        float dist = (Cam.transform.position.x * ParallaxEffect);

        transform.position = new Vector3(startPos + dist, transform.position.y, transform.position.z);
	}
}
