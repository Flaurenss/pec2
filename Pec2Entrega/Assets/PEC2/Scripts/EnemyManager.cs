﻿using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public float enemySpeed = 1;
    public Transform feetPos;
    public GameObject rightLimit;
    public GameObject leftLimit;
    public GameObject punctuationText;
    public LayerMask whatIsGround;
    public bool killed = false;

    Animator animator;
    Rigidbody2D enemyRigidBody2D;
    private bool isActivated = false;
    private bool moveRight = false;
    private bool dead;
    public float groundCheckRadius = 0.169f;
    private bool onGround;

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        enemyRigidBody2D = GetComponent<Rigidbody2D>();
        CheckOrientation();
        dead = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (killed)
            EnemyDead();
        onGround = CheckOnGround();
	}

    void FixedUpdate()
    {
        //Test pourposes
        // isActivated = false;
        //
        if (isActivated && !dead)
        {
            CheckWallLimits();

            if (moveRight && onGround)
            {
                enemyRigidBody2D.velocity = new Vector2(transform.right.x * enemySpeed, transform.right.y);
            }

            if (!moveRight && onGround)
            {
                enemyRigidBody2D.velocity = new Vector2(-transform.right.x * enemySpeed, transform.right.y);
            }
        }
    }

    private bool CheckOnGround()
    {
        if (Physics2D.OverlapCircleAll(feetPos.position, groundCheckRadius, whatIsGround).Length > 0)
        {
            animator.SetBool("OnGround",true);
            return true;
        }
        else
        {
            animator.SetBool("OnGround", false);
            return false;
        }
    }

    private void CheckOrientation()
    {
        if(transform.localScale.x == 1)
        {
            moveRight = false;
        }
        else
        {
            moveRight = true;
        }
    }

    private void Flip()
    {
        var scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
        //Change orientation of child PunctuationText in order to show it properly
        var scalePunctuation = punctuationText.transform.localScale;
        scalePunctuation.x *= -1;
        punctuationText.transform.localScale = scalePunctuation;
    }

    /// <summary>
    /// All enemies start from right limit to left limit
    /// </summary>
    void CheckWallLimits()
    {
        if(feetPos.transform.position.x <= leftLimit.transform.position.x)
        {
            moveRight = !moveRight;
            Flip();
        }
        else if(feetPos.transform.position.x >= rightLimit.transform.position.x)
        {
            moveRight = !moveRight;
            Flip();
        }
    }

    private void OnBecameVisible()
    {
        //Start patrol
        isActivated = true;
    }

    private void OnBecameInvisible()
    {
        //Stop patrol
        isActivated = false;
    }

    public void EnemyDead()
    {
        GetComponentInChildren<BoxCollider2D>().enabled = false;
        GetComponent<CapsuleCollider2D>().enabled = false;
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        dead = true;
        animator.SetTrigger("EnemyDeath");
    }

    public void DestroyEnemy()
    {
        Destroy(gameObject);
    }

    /// <summary>
    /// If the enemy collided with another enemy, flip directions
    /// </summary>
    /// <param name="other"></param>
    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            Flip();
            CheckOrientation();
        }
    }
}
