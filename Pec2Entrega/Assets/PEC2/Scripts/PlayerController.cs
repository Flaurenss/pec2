﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float fallMultiplier = 5f;
    public float gravity = 1f;
    public Vector2 direction;
    public float linearDrag = 4f;
    public float linealVelocity = 10f;
    public Animator animator;
    public Transform feetPos;
    public float jumpForce = 6f;
    public float jumpTime = 0.25f;
    public LayerMask whatIsGround;
    public float groundCheckRadius = 0.13f;
    public float maxRunSpeed = 5f;
    public CapsuleCollider2D triggerCapusleCollider2D;
    public delegate void finishDelegate();
    public event finishDelegate finishLevel;
    public event finishDelegate dead;
    public AudioClip jumpSound;
    public AudioClip deathSound;
    public bool killed;
    public static bool finalStage;

    private Rigidbody2D rigidBody;
    private bool facingRight = true;
    private bool facingLeft = false;
    public float velocity;
    private Vector3 previousPos;
    public bool stoppedJumping;
    public bool grounded;
    public float jumpTimeCounter;
    private AudioSource audioSource;
    public bool isJumping = false;
    private bool landing = false;
    public bool canJump = true;

    void Start () {
        stoppedJumping = true;
        finalStage = false;
        killed = false;
        audioSource = GetComponent<AudioSource>();
        rigidBody = GetComponent<Rigidbody2D>();
        jumpTimeCounter = jumpTime;
    }
    void Update()
    {
        //There is the need to check on every update if the user is jumping in order to avoid pressing key issues
        // ModifyPhysiscs();
        HandleAirMovements();
        grounded = CheckOnGround();
        if (grounded)
        {
            isJumping = false;
            animator.ResetTrigger("Jump");
            animator.SetBool("Land", false);
            jumpTimeCounter = jumpTime;

            if (Input.GetKey(KeyCode.Space) && landing)
            {
                // residualV = new Vector2(rigidBody.velocity.x, 0f);
                canJump = false;
                jumpTimeCounter = 0;
            }
            else if (!Input.GetKey(KeyCode.Space) && landing)
            {
                landing = false;
                canJump = true;
            }
        }
        direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
    }
    void ModifyPlayerRbValues()
    {
        bool changeDirection = (direction.x > 0 && rigidBody.velocity.x < 0) || (direction.x < 0f && rigidBody.velocity.x > 0f);
        if(grounded)
        {
            if(Mathf.Abs(direction.x) < 0.4f || changeDirection)
            {
                rigidBody.drag = linearDrag;
            }
            else
            {
                rigidBody.drag = 0f;
            }
            rigidBody.gravityScale = 1f;
        }
        else
        {
            rigidBody.gravityScale = gravity;
            rigidBody.drag = linearDrag * 0.15f;
            if(rigidBody.velocity.y < 0)
            {
                rigidBody.gravityScale = gravity * fallMultiplier;
            }
            else if(rigidBody.velocity.y > 0 && !Input.GetKey(KeyCode.Space))
            {
                rigidBody.gravityScale = gravity * (fallMultiplier / 2);
            }
        }
    }
    /// <summary>
    /// Check if the player is moving (not done in normal update in order to not benify better performance = more fast)
    /// </summary>
    private void FixedUpdate()
    {
        HandleGroundInput(direction.x);
        ModifyPlayerRbValues();
        HandleLayers();
    }

    /// <summary>
    /// Not used
    /// </summary>
    // void HandleBodyDrag()
    // {
    //     if(grounded)
    //     {
    //         if((Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.LeftArrow)) || (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D)))
    //         {
    //             rigidBody.velocity = new Vector2(Mathf.Sign(rigidBody.velocity.x) * 0, rigidBody.velocity.y);
    //         }

    //         if(!Input.GetKey(KeyCode.RightArrow) || !Input.GetKey(KeyCode.D))
    //         {
    //             rigidBody.drag = 15f;
    //         }
    //         else if(!Input.GetKey(KeyCode.LeftArrow) || !Input.GetKey(KeyCode.A))
    //         {
    //             rigidBody.drag = 15f;
    //         }
    //         if((Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow)) || 
    //             (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A)))
    //         {
    //             rigidBody.drag = 0f;
    //         }
    //     }
    //     else
    //     {
    //         rigidBody.drag = 0f;
    //     }
    // }

    bool CheckOnGround()
    {
        //Debug.Log(Physics2D.OverlapCircleAll(feetPos.position, groundCheckRadius, whatIsGround).Length);
        if(Physics2D.OverlapCircleAll(feetPos.position, groundCheckRadius, whatIsGround).Length > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        // Gizmos.color = Color.yellow;
        // Gizmos.DrawSphere(feetPos.position, groundCheckRadius);
    }

    void HandleGroundInput(float horizontal)
    {
        rigidBody.AddForce(Vector2.right * horizontal * linealVelocity);
        if((horizontal > 0 && !facingRight) || horizontal < 0 && facingRight)
        {
            animator.SetBool("ChangingPosition", true);
        }
        if(Mathf.Abs(rigidBody.velocity.x) > maxRunSpeed)
        {
            rigidBody.velocity = new Vector2(Mathf.Sign(rigidBody.velocity.x) * maxRunSpeed, rigidBody.velocity.y);
        }
        animator.SetFloat("Speed", Mathf.Abs(rigidBody.velocity.x));
    }
    void HandleAirMovements()
    {
        #region AirMovements
        if (Input.GetKeyDown(KeyCode.Space) && grounded && canJump)
        {
            isJumping = true;
            audioSource.clip = jumpSound;
            audioSource.Play();
            stoppedJumping = false;
            rigidBody.velocity = new Vector2(rigidBody.velocity.x,jumpForce);
            // rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0);
            // rigidBody.AddForce(Vector3.up * jumpForce, ForceMode2D.Impulse);
        }

        //If keeps pressing space keeps jumping
        if (Input.GetKey(KeyCode.Space) && !stoppedJumping)
        {
            if (jumpTimeCounter > 0 && canJump && !grounded)
            {
                isJumping = true;
                // rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0);
                // rigidBody.AddForce(Vector3.up * jumpForce, ForceMode2D.Impulse);
                rigidBody.velocity = new Vector2(rigidBody.velocity.x, jumpForce);
                jumpTimeCounter -= Time.deltaTime;
                animator.SetTrigger("Jump");
            }
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            jumpTimeCounter = 0;
            stoppedJumping = true;
        }
        if (rigidBody.velocity.y < 0)
        {
            animator.SetBool("Land", true);
            landing = true;
        }
        #endregion
    }

    /// <summary>
    /// Change position of the player when the x direction chenges
    /// Called from animation Player_ChangeDirection
    /// </summary>
    public void ChangePositionAnimationEnd()
    {
        animator.SetBool("ChangingPosition", false);
        if(facingRight)
        {
            var scale = transform.localScale;
            scale.x = -1;
            transform.localScale = scale;
            facingRight = false;
            facingLeft = true;
        }
        else
        {
            var scale = transform.localScale;
            scale.x = 1;
            transform.localScale = scale;
            facingRight = true;
            facingLeft = false;
        }
    }

    /// <summary>
    /// Checks if the user is on ground in order to give access to a group of animations or another
    /// </summary>
    private void HandleLayers()
    {
        if (!grounded)
        {
            //show jump,land animations
            animator.SetLayerWeight(1, 1);
        }
        else
        {
            animator.SetLayerWeight(1, 0);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy" && !collision.gameObject.GetComponent<EnemyManager>().killed)
        {
            Debug.Log("Collision enter");
            PlayerDead();
        }
    } 

    void OnTriggerEnter2D(Collider2D other)
    {
        if( other.gameObject.tag == "DeadZone")
        {
            PlayerDead();
        }
        else if(other.gameObject.tag == "KeyItem" && grounded)
        {
            if(finishLevel != null)
            {
                finalStage = true;
                enabled = false;
                FinalItemManager.isAchieved = true;
                animator.SetTrigger("KeyItem");
                rigidBody.bodyType = RigidbodyType2D.Static;
                finishLevel();
            }
        }
    }

    void PlayerDead()
    {
        if(dead != null)
        {
            Debug.Log("Mickey dead");
            killed = true;
            audioSource.clip = deathSound;
            audioSource.Play();
            // GetComponentInChildren<CircleCollider2D>().enabled = false;
            // GetComponentInChildren<BoxCollider2D>().enabled = false;
            GetComponent<CapsuleCollider2D>().enabled = false;
            GetComponent<EdgeCollider2D>().enabled = false;
            rigidBody.bodyType = RigidbodyType2D.Static;
            dead();
        }
    }

    public void EnemyKilled()
    {
        audioSource.clip = jumpSound;
        audioSource.Play();
        //When the enemy has been killed, make jump the player (short jump)
        rigidBody.velocity = new Vector2(rigidBody.velocity.x, jumpForce);
    }
}
