﻿using UnityEngine;

public class FinalItemManager : MonoBehaviour {
    public Animator Animator;
    public static bool isAchieved;

    private PlayerController mickeyController;

	// Use this for initialization
	void Start () {
        isAchieved = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(isAchieved)
        {
            Animator.SetTrigger("ItemAchieved");
        }
	}

    public void GetPunctuation()
    {
        GamePlay.punctuation += 500;
    }
}
