﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFeetPosManager : MonoBehaviour {

    public LayerMask layerToCheck;
    public float xOffsetRayValueC;
    public float xOffsetRayValueL;
    public float xOffsetRayValueR;
    public float yOffset;
    Color drawColor = Color.yellow;
    private PlayerController playerController;
    private bool isDead = false;
    void Start()
    {
        playerController = GetComponentInParent<PlayerController>();
        playerController.dead += PlayerDead;
    }

    void PlayerDead()
    {
        isDead = true;
    }
    void Update()
    {
        if(!isDead)
        {
            Debug.DrawRay(new Vector2(transform.position.x + xOffsetRayValueC, transform.position.y + 0.2f), Vector2.down * yOffset, drawColor);
            Debug.DrawRay(new Vector2(transform.position.x + xOffsetRayValueR, transform.position.y + 0.2f), Vector2.down * yOffset, drawColor);
            Debug.DrawRay(new Vector2(transform.position.x + xOffsetRayValueL, transform.position.y + 0.2f), Vector2.down * yOffset, drawColor);
        }
    }
    void FixedUpdate()
    {
        if(!isDead)
        {
            RaycastHit2D centerHit = Physics2D.Raycast(new Vector2(transform.position.x + xOffsetRayValueC, transform.position.y), Vector2.down, yOffset, layerToCheck);
            RaycastHit2D rightHit = Physics2D.Raycast(new Vector2(transform.position.x + xOffsetRayValueR, transform.position.y), Vector2.down, yOffset, layerToCheck);
            RaycastHit2D leftHit = Physics2D.Raycast(new Vector2(transform.position.x + xOffsetRayValueL, transform.position.y), Vector2.down, yOffset, layerToCheck);
            if(centerHit.collider != null)
            {
                KillEnemy(centerHit);
            }
            else if(rightHit.collider != null)
            {
                KillEnemy(rightHit);
            }
            else if(leftHit.collider != null)
            {
                KillEnemy(leftHit);
            }
            else
            {
                drawColor = Color.yellow;
            }
        }
    }
    void KillEnemy(RaycastHit2D hit)
    {
        Debug.Log("Hits enemy with raycast");
        hit.collider.gameObject.GetComponentInParent<EnemyManager>().killed = true;
        drawColor = Color.red;
        Debug.Log("Hit Enemy by feets");
        GetComponentInParent<PlayerController>().EnemyKilled();
        GamePlay.punctuation += 100;
    }
}
