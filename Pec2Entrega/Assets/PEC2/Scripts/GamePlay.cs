﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlay : MonoBehaviour {

    public PlayerController character;
    public static int lifes = 3;
    public static int gems = 0;
    public static int punctuation = 0;
    public int time;
    public Text lifeNumber;
    public Text gemNumber;
    public Text punctuationNumber;
    public Text timeNumber;
    public GameObject audioManager;
    public AudioClip counterPunctuation;
    public static bool resetCanvasValues;

    private Animator animator;
    private AudioSource audioSource;

	// Use this for initialization
	void Start ()
    {
        resetCanvasValues = false;
        time = 400;
        SetCanvasValues();
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        character.dead += PlayerDeath;
        character.finishLevel += StageEnd;
        InvokeRepeating("TimeCounter", 1, 1);
    }
	
	// Update is called once per frame
	void Update ()
    {
        SetCanvasValues();
        if(resetCanvasValues)
        {
            ResetCanvasValues();
        }
	}

    void TimeCounter()
    {
        if(!character.killed && !FinalItemManager.isAchieved)
        {
            time--;
            timeNumber.text = time.ToString();
            if (time == 0)
            {
                //GameOver
                SceneManager.LoadScene("GameOverScene");
            }
        }
    }

    /// <summary>
    /// Parse actual values in order to get reflected on the UI
    /// </summary>
    void SetCanvasValues()
    {
        lifeNumber.text = lifes.ToString();
        gemNumber.text = gems.ToString();
        punctuationNumber.text = punctuation.ToString();
        timeNumber.text = time.ToString();
    }

    void PlayerDeath()
    {
        character.enabled = false;
        animator.SetBool("Death", true);
    }
    /// <summary>
    /// Executed when the player animation is finished
    /// </summary>
    void FinishDeath()
    {
        lifes--;
        animator.SetBool("Death", false);
        if (lifes == 0)
        {
            //Display gameover scene
            SceneManager.LoadScene("GameOverScene");
            ResetCanvasValues();
        }
        else
        {
            SceneManager.LoadScene("GameScene");
        }
    }

    public void ResetCanvasValues()
    {
        lifes = 3;
        gems = 0;
        punctuation = 0;
    }

    void StageEnd()
    {
        //Add points from every second not spend
        audioSource.clip = counterPunctuation;
        InvokeRepeating("TimeCounterToPoints", 1, 0.03f);
    }

    void TimeCounterToPoints()
    {
        if(time != 0)
        {
            audioSource.Play();
            time--;
            punctuation += 10;
        }
        else
        {
            CancelInvoke();
            //Show winer scene
            VictoryController.finalCountPunctuation = true;
        }
    }
}
